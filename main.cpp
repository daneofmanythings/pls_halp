#include "base.h"
#include "container.h"
#include "derived.h"

#include <iostream>

int main() {
  Base *d = new Derived{};
  std::cout << *d << std::endl;
  d->add_one();
  std::cout << *d << std::endl;
  Container c{};
  Derived d2{};
  c.add_point(d2);
  std::cout << c << std::endl;
}
