#ifndef _PRINTERFACE_H_
#define _PRINTERFACE_H_

#include <iostream>

class Printerface {
  friend std::ostream &operator<<(std::ostream &os, const Printerface &obj);

private:
protected:
public:
  virtual void print_(std::ostream &os) const = 0;
  virtual ~Printerface() = default;
};

#endif // !_PRINTERFACE_H_
