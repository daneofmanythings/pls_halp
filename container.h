#ifndef _CONTAINER_H_
#define _CONTAINER_H_

#include "base.h"
#include "derived.h"
#include "printerface.h"
#include <vector>

class Container : public Printerface {
private:
  std::vector<Base *> *points;

protected:
public:
  void add_point(const Derived d);
  void print_(std::ostream &os) const override;
  Container() : points{new std::vector<Base *>{}} {};
  virtual ~Container() {
    for (auto &ptr : *points) {
      delete ptr;
    }
    delete points;
  }
};

#endif // !_CONTAINER_H_
