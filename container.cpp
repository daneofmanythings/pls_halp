#include "container.h"

void Container::add_point(const Derived p) {
  Derived *ptr = new Derived;
  *ptr = p;
  points->push_back(ptr);
}

void Container::print_(std::ostream &os) const {
  for (const auto &p : *points) {
    os << *p;
  }
}
