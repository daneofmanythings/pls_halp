#ifndef _BASE_H_
#define _BASE_H_

#include "printerface.h"

class Base : public Printerface {
private:
protected:
  int x;

public:
  virtual void add_one() = 0;
  virtual void print_(std::ostream &os) const = 0;

  Base(int x) : x{x} {};
  virtual ~Base() = default;
};

#endif // !_BASE_H_
