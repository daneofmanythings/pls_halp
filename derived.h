#ifndef _DERIVED_H_
#define _DERIVED_H_

#include "base.h"

class Derived : public Base {
private:
  int y;
  static constexpr int default_int = 0;

protected:
public:
  void add_one() override;
  void print_(std::ostream &os) const override;
  Derived(int x = default_int, int y = default_int) : Base{x}, y{y} {};
  virtual ~Derived() = default;
};

#endif // !_DERIVED_H_
