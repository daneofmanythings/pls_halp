#include "printerface.h"

std::ostream &operator<<(std::ostream &os, const Printerface &obj) {
  obj.print_(os);
  return os;
}
