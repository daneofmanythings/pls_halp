#include "derived.h"

void Derived::add_one() {
  x += 1;
  y += 1;
}

void Derived::print_(std::ostream &os) const {
  os << "(x=" << x << ", y=" << y << ")";
}
